cmake_minimum_required(VERSION 3.8)
project(load_pose_stamped)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# Find dependencies
find_package(ament_cmake REQUIRED)
find_package(std_msgs REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(rosidl_default_generators REQUIRED)

# Generate the interfaces for your custom messages
rosidl_generate_interfaces(${PROJECT_NAME}
  "msg/LoadPose.msg"
  "msg/LoadPoseStamped.msg"
  DEPENDENCIES std_msgs geometry_msgs
)

ament_package()
